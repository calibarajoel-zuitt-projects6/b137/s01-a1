package com.zuitt.activity;

public class Activity {
    public static void main(String[] args){
        String firstName;
        firstName = "Joel";

        String lastName;
        lastName = "Calibara";

        int gradeInEnglish;
        gradeInEnglish = 80;

        int gradeInScience;
        gradeInScience = 85;

        int gradeInMath;
        gradeInMath = 93;

        int average;
        average = (gradeInEnglish + gradeInMath + gradeInScience)/3;

        System.out.println("Student Name: "+ firstName +" "+ lastName);
        System.out.println("Average grade: " + average);

    }
}

